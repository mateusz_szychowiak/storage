**Instrukcja instalacji**

-git clone git@bitbucket.org:mateusz_szychowiak/storage.git  
-cd storage  
-composer install  
-cp .env.example .env  
-w pliku .env należy podmienić zmienne na odpowiadające danemu środowisku  
-php artisan key:generate  
-php artisan migrate (wymaga stworzonej bazy storage)  