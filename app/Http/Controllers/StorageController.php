<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Price;

class StorageController extends Controller {


    public function createView() {
        return view('create.index');
    }

    public function createProduct(Request $request) {
        $success = false;

        $name = $request->input('name');
        $description = $request->input('description');

        $pricesValue = explode(',', $request->input('prices'));

        foreach ($pricesValue as $pv) {
            if (floatval($pv) == 0) {
                return view('create.index', [
                    'success' => $success
                ]);
            }
        }
        if (empty($name) || empty($description)) {
            return view('create.index', [
                'success' => $success
            ]);
        }

        $product = new Product();

        $product->name = $name;
        $product->description = $description;

        $product->save();

        foreach ($pricesValue as $pv) {
            $prices = new Price();
            $prices->price = $pv;
            $prices->product_id = $product->id;
            $prices->save();
        }

        $success = true;

        return view('create.index', [
            'success' => $success
        ]);
    }

    public function editView($id) {
        $product = Product::where('id', '=', $id)->first();
        if (empty($product)) {
            return view('edit.404');
        }
        $prices = Price::where('product_id', '=', $id)->get()->toArray();
        $pricesValue = [];
        foreach ($prices as $key => $price) {
            $pricesValue[$key] = $price['price'];
        }
        return view('edit.index', [
            'product' => $product,
            'prices' => implode(',', $pricesValue)
        ]);
    }

    public function editProduct($id, Request $request) {
        $success = false;
        $name = $request->input('name');
        $description = $request->input('description');

        $pricesValue = explode(',', $request->input('prices'));

        foreach ($pricesValue as $pv) {
            if (floatval($pv) == 0) {
                return view('edit.index', [
                    'success' => $success
                ]);
            }
        }
        if (empty($name) || empty($description)) {
            return view('edit.index', [
                'success' => $success
            ]);
        }

        $product = Product::where('id', '=', $id)->first();

        $product->name = $name;
        $product->description = $description;

        $product->save();

        $product->prices()->forceDelete();

        foreach ($pricesValue as $pv) {
            $prices = new Price();
            $prices->price = $pv;
            $prices->product_id = $product->id;
            $prices->save();
        }

        $success = true;
        return view('edit.index', [
            'product' => $product,
            'prices' => implode(',', $pricesValue),
            'success' => $success
        ]);
    }

    public function deleteProduct($id) {
        $product = Product::where('id', '=', $id)->first();
        $product->deleted = date('Y-m-d H:i:s');
        $product->save();
    }

}
