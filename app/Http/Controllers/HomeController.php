<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Price;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $products = Product::where('deleted', '=', null)->get()->toArray();
        foreach ($products as $key => $product) {
            $prices = Price::where('product_id', '=', $product['id'])->get()->toArray();
            $pricesValue = [];
            foreach ($prices as $keyPrice => $price) {
                $pricesValue[$keyPrice] = $price['price'];
            }
            $products[$key]['prices'] = implode(', ', $pricesValue);
        }

        return view('home', [
            'products' => $products
        ]);
    }

}
