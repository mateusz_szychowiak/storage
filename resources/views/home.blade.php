@extends('layouts.app')

@section('content')
<div class="token hidden" id="{{ csrf_token() }}"></div>
<div class='warning-info hidden'>
    <p>Jesteś pewien?</p>
    <button id="remove" class="btn btn-danger">Usuń</button>
    <button id="cancel" class='btn btn-info'>Zamknij</button>
</div>
<div class="container">
     <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nazwa</th>
                        <th>Opis</th>
                        <th>Ceny</th>
                        <th>Data stworzenia</th>
                        <th>Data modyfikacji</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                    <tr id="{{$product['id']}}">
                        <td>{{$product['id']}}</td>
                        <td>{{$product['name']}}</td>
                        <td>{{$product['description']}}</td>
                        <td>{{$product['prices']}}</td>
                        <td>{{$product['created_at']}}</td>
                        <td>{{$product['updated_at']}}</td>
                        <td><a href="/edit/{{$product['id']}}" class="btn btn-info">Edytuj</a></td>
                        <td><button id="{{$product['id']}}" class="btn btn-danger delete-btn">Usuń</button></td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Nazwa</th>
                        <th>Opis</th>
                        <th>Ceny</th>
                        <th>Data stworzenia</th>
                        <th>Data modyfikacji</th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
