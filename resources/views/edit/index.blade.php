@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if(isset($success))
            @if(($success))
            <div class="alert alert-info">
                Pomyślnie edytowano produkt!                
            </div>
            @else
            <div class="alert alert-danger">
                Coś poszło nie tak!               
            </div>
            @endif
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">Edytuj produkt!</div>

                <div class="panel-body">
                    <form method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label >Nazwa</label>
                            <input type="text" class="form-control" name="name" value="{{$product['name']}}" placeholder="Podaj nazwę produktu">
                            <div id="error-name" class="error-info hidden">Nazwa nie może być pusta!</div>
                        </div>
                        <div class="form-group">
                            <label >Opis</label>
                            <input type="text" class="form-control" name="description" value="{{$product['description']}}"  placeholder="Podaj opis produktu">
                            <div id="error-description" class="error-info hidden">Opis nie może być pusty!</div>                        
                        </div>
                        <div class="form-group">
                            <label >Ceny</label>
                            <input class="tags-input" class="form-control" name="prices" data-role="tagsinput" value="{{$prices}}" placeholder="Podaj ceny">
                        <div id="error-prices" class="error-info hidden">Ceny nie mogą być puste</div>
                            <div id="error-prices-format" class="error-info hidden">Cena musi być liczbą, maksymalnie do dwóch zer po kropce!</div>
                        </div>
                        <button id="sub-form" class="btn btn-primary">Dodaj</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
