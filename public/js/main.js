$(document).ready(function () {

    $('.delete-btn').click(function () {
        var id = this['id'];
        $('.warning-info').removeClass('hidden');
        $('#remove').click(function () {
            $.post('/delete/' + id, {_token: $('.token').attr('id')})
                    .done(function () {
                        $('.warning-info').addClass('hidden');
                        location.reload();
                    });
        });
        $('#cancel').click(function () {
            $('.warning-info').addClass('hidden');
        });
    });

    $('input[name=name]').focusout(function () {
        if (this['value'] === '') {
            $('input[name=name]').addClass('error');
            $('#error-name').removeClass('hidden');
        } else {
            $('input[name=name]').removeClass('error');
            $('#error-name').addClass('hidden');
        }
    });
    $('input[name=description]').focusout(function () {
        if (this['value'] === '') {
            $('input[name=description]').addClass('error');
            $('#error-description').removeClass('hidden');
        } else {
            $('input[name=description]').removeClass('error');
            $('#error-description').addClass('hidden');
        }
    });
    $('.bootstrap-tagsinput').focusout(function () {
        if ($('input[name=prices]').val() === '') {
            $('.bootstrap-tagsinput').addClass('error');
            $('#error-prices').removeClass('hidden');
        } else {
            $('.bootstrap-tagsinput').removeClass('error');

            $('#error-prices').addClass('hidden');
        }
    });

    $('#sub-form').click(function () {
        event.preventDefault();
    });
    $('#sub-form').mouseup(function () {
        var error = validateForm();
        if (!error) {
            console.log(error);
            $('form').submit();
        }
    });

    var table = $('#example').DataTable();
});

function validateForm() {
    var error = false;
    if ($('input[name=name]').val() === '') {
        $('input[name=name]').addClass('error');
        $('#error-name').removeClass('hidden');
        error = true;
    }
    if ($('input[name=description]').val() === '') {
        $('input[name=description]').addClass('error');
        $('#error-description').removeClass('hidden');
        error = true;
    }
    if ($('input[name=prices]').val() === '') {
        $('.bootstrap-tagsinput').addClass('error');
        $('#error-prices').removeClass('hidden');
        error = true;
    } else {
        var array = $('input[name=prices]').val().split(',');
        for (var i = 0; i < array.length; i++) {
            var nr = parseFloat(array[i]);
            if (isNaN(nr) || countDecimals(nr) > 2 || array[i] === "0") {
                $('#error-prices-format').removeClass('hidden');
                error = true;
            }
        }
    }

    return error;
}

function countDecimals(n) {
    if (Math.floor(n.valueOf()) === n.valueOf())
        return 0;
    return n.toString().split(".")[1].length || 0;
}
